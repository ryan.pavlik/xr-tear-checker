# Copyright 2019, Collabora, Ltd.
#
# SPDX-License-Identifier: BSL-1.0

add_executable(
  OpenXRTearChecker
  OpenXRTearChecker.cpp OpenXRPlatformIncludeWrapper.h swapchain_image_data.cpp
  swapchain_image_data.h graphics_plugin.h)
target_link_libraries(OpenXRTearChecker PUBLIC OpenXR::Loader Threads::Threads
                                               fmt openxr-hpp)

target_compile_definitions(OpenXRTearChecker PRIVATE XR_USE_TIMESPEC)
target_include_directories(OpenXRTearChecker
                           PRIVATE ../vendor/span-lite/include)

if(NOT MSVC)
  target_compile_options(OpenXRTearChecker PRIVATE -Wno-old-style-cast)
endif()
if(BUILD_WITH_OPENGL_XLIB)
  target_compile_definitions(OpenXRTearChecker
                             PRIVATE XR_USE_GRAPHICS_API_OPENGL)

  target_compile_definitions(OpenXRTearChecker PRIVATE XR_USE_PLATFORM_XLIB)
endif()
if(BUILD_WITH_VULKAN)
  target_compile_definitions(OpenXRTearChecker
                             PRIVATE XR_USE_GRAPHICS_API_VULKAN)
endif()
if(BUILD_WITH_D3D11)
  target_compile_definitions(OpenXRTearChecker
                             PRIVATE XR_USE_GRAPHICS_API_D3D11)
  target_sources(OpenXRTearChecker PRIVATE graphics_plugin_d3d11.cpp)
  target_link_libraries(OpenXRTearChecker PRIVATE d3d11)
endif()

if(WIN32)
  target_compile_definitions(OpenXRTearChecker PRIVATE XR_USE_PLATFORM_WIN32
                                                       WIN32_LEAN_AND_MEAN)
  target_sources(OpenXRTearChecker PRIVATE d3d_common.cpp)
  target_link_libraries(OpenXRTearChecker PRIVATE dxgi)
endif()

install(TARGETS OpenXRTearChecker RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
                                          COMPONENT Runtime)
