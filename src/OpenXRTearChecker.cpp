// Copyright 2019-2023, Collabora, Ltd.
// Copyright 2017-2022, The Khronos Group Inc.
//
// SPDX-License-Identifier: Apache-2.0
/*!
 * @file
 * @brief  Implementation
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

// Internal Includes
#include "OpenXRPlatformIncludeWrapper.h"
#include "graphics_plugin.h"
#include "openxr/openxr_duration.hpp"
#include "openxr/openxr_enums.hpp"
#include "openxr/openxr_flags.hpp"
#include "openxr/openxr_handles_forward.hpp"
#include "swapchain_image_data.h"
#include "throw_helpers.h"

// Library Includes
#include "openxr/openxr_atoms.hpp"
#include "openxr/openxr_dispatch_dynamic.hpp"
#include "openxr/openxr_handles.hpp"
#include "openxr/openxr_method_impls.hpp"
#include "openxr/openxr_structs.hpp"

// Standard Includes
#include <algorithm>
#include <dxgi.h>
#include <dxgiformat.h>
#include <intsafe.h>
#include <iomanip>
#include <iostream>
#include <openxr/openxr.h>
#include <stdexcept>
#include <string>
#include <thread>
#include <vector>

// #define FORCE_API_DUMP
// #define FORCE_VALIDATION

#ifdef _WIN32
#include <dxgi1_4.h>
#include <wrl.h>
#include <wrl/client.h>

#endif

#ifdef XR_USE_GRAPHICS_API_D3D11
#include <d3d11.h>
#endif

using namespace Microsoft::WRL;

static const uint32_t numViews_ = 2;

xr::SessionState state = xr::SessionState::Unknown;

static bool hasExtension(std::vector<xr::ExtensionProperties> const &extensions, const char *name) {
    auto b = extensions.begin();
    auto e = extensions.end();

    const std::string name_string = name;

    return (e !=
            std::find_if(b, e, [&](const xr::ExtensionProperties &prop) { return prop.extensionName == name_string; }));
}

template <typename Dispatch> void pollEvent(xr::Instance instance, Dispatch &&d) {
    while (1) {
        xr::EventDataBuffer event;
        auto result = instance.pollEvent(event, d);
        if (result == xr::Result::EventUnavailable) {
            return;
        } else if (result != xr::Result::Success) {
            std::cout << "Got error polling for events: " << to_string(result) << std::endl;
            return;
        }

        std::cout << "Event: " << to_string_literal(event.type) << std::endl;
        if (event.type == xr::StructureType::EventDataSessionStateChanged) {
            auto &change = reinterpret_cast<xr::EventDataSessionStateChanged &>(event);
            std::cout << to_string(change.state) << std::endl;
            state = change.state;
        }
    }
}
enum class Backend {
    None,
    // Headless,
    // OpenGL,
    // Vulkan,
    D3D11,
};

int main(int /* argc */, char * /* argv */[]) {

    xr::UniqueDynamicInstance instance;
    auto d = xr::DispatchLoaderDynamic{};

    Backend backend = Backend::None;

    // Set up instance.
    {
        // Using the openxr.hpp wrapper that performs the whole two-call operation in one step
        auto apiLayers = xr::enumerateApiLayerPropertiesToVector(d);
        std::cout << "Enumerated layers:\n";
        for (const auto &prop : apiLayers) {
            std::cout << prop.layerName << " - " << prop.description << std::endl;
        }
        std::cout << "Number of layers: " << apiLayers.size() << std::endl;

        std::vector<const char *> enabledLayers;

        // Using the openxr.hpp wrapper that performs the whole two-call operation in one step
        auto extensions = xr::enumerateInstanceExtensionPropertiesToVector(nullptr, d);

        std::vector<const char *> enabledExtensions;

        if (hasExtension(extensions, XR_KHR_D3D11_ENABLE_EXTENSION_NAME)) {
            std::cout << "Using D3D11" << std::endl;
            backend = Backend::D3D11;
            enabledExtensions.push_back(XR_KHR_D3D11_ENABLE_EXTENSION_NAME);
        } else {
            throw std::runtime_error("Could not find enough extensions to make a session!");
        }

        instance = xr::createInstanceUnique(
            xr::InstanceCreateInfo{
                xr::InstanceCreateFlagBits::None,
                xr::ApplicationInfo{"OpenXR Print Tracker", 1, // app version
                                    "", 0,                     // engine version
                                    xr::Version::current()},
                uint32_t(enabledLayers.size()),
                enabledLayers.data(),
                uint32_t(enabledExtensions.size()),
                enabledExtensions.data(),
            },
            d);
    }

    // Update the dispatch now that we have an instance
    d = xr::DispatchLoaderDynamic::createFullyPopulated(instance.get(), &::xrGetInstanceProcAddr);

    pollEvent(instance.get(), d);
    // Spawn a thread to wait for a keypress
    static bool quitKeyPressed = false;

    auto exitPollingThread = std::thread{[] {
        std::cout << "Press any key to shutdown...\n";
        (void)getchar();
        quitKeyPressed = true;
    }};
    exitPollingThread.detach();

    xr::UniqueDynamicSession session;

    std::shared_ptr<IGraphicsPlugin> graphicsPlugin;
    auto viewConfigType = xr::ViewConfigurationType::PrimaryStereo;
    std::vector<xr::ViewConfigurationView> viewConfigViews;
    std::vector<xr::EnvironmentBlendMode> blendModes;
    int64_t swapchainFormat = 0;
    // Get system, set up session
    {
        xr::SystemId systemId = instance->getSystem(xr::SystemGetInfo{xr::FormFactor::HeadMountedDisplay}, d);

        xr::SessionCreateInfo createInfo{xr::SessionCreateFlagBits::None, systemId};
        viewConfigViews = instance->enumerateViewConfigurationViewsToVector(systemId, viewConfigType, d);

#if defined(XR_USE_GRAPHICS_API_OPENGL) && defined(XR_USE_PLATFORM_XLIB)
        xr::GraphicsBindingOpenGLXlibKHR glXlibBinding;
        if (backend == Backend::OpenGL) {
            auto requirements = instance->getOpenGLGraphicsRequirementsKHR(systemId, d);
            (void)requirements;
            createInfo.next = get(glXlibBinding);
        }
#endif // defined(XR_USE_GRAPHICS_API_OPENGL) && defined(XR_USE_PLATFORM_XLIB)
#ifdef XR_USE_GRAPHICS_API_VULKAN
        xr::GraphicsBindingVulkanKHR vulkanBinding;
        if (backend == Backend::Vulkan) {
            auto requirements = instance->getVulkanGraphicsRequirementsKHR(systemId, d);
            (void)requirements;
            createInfo.next = get(vulkanBinding);
        }
#endif // XR_USE_GRAPHICS_API_VULKAN
#ifdef XR_USE_GRAPHICS_API_D3D11
        if (backend == Backend::D3D11) {
            graphicsPlugin = CreateGraphicsPlugin_D3D11();
            if (!graphicsPlugin->InitializeDevice(instance.get(), systemId.get())) {
                return XR_ERROR_RUNTIME_FAILURE;
            }
            swapchainFormat = DXGI_FORMAT_R8G8B8A8_UNORM_SRGB;
        }
#endif // XR_USE_GRAPHICS_API_D3D11
        createInfo.next = graphicsPlugin->GetGraphicsBinding();

        blendModes = instance->enumerateEnvironmentBlendModesToVector(systemId, viewConfigType, d);
        session = instance->createSessionUnique(createInfo, d);
    }

    const uint32_t numViews = static_cast<uint32_t>(viewConfigViews.size());
    ISwapchainImageData *imageData;
    xr::UniqueDynamicSwapchain swapchain;
    xr::Rect2Di rect;
    {
        const auto &vcv = viewConfigViews.front();
        rect.extent.height = vcv.recommendedImageRectHeight;
        rect.extent.width = vcv.recommendedImageRectWidth;

        auto sci = xr::SwapchainCreateInfo{xr::SwapchainCreateFlagBits::None,
                                           xr::SwapchainUsageFlagBits::ColorAttachment,
                                           swapchainFormat,
                                           1,
                                           vcv.recommendedImageRectWidth,
                                           vcv.recommendedImageRectHeight,
                                           1,
                                           numViews,
                                           1};
        swapchain = session->createSwapchainUnique(sci, d);
        uint32_t numImages = 0;
        XRC_CHECK_THROW_XRCMD(get(swapchain->enumerateSwapchainImages(0, &numImages, nullptr)));

        imageData = graphicsPlugin->AllocateSwapchainImageData(numImages, sci);
        XRC_CHECK_THROW_XRCMD(
            get(swapchain->enumerateSwapchainImages(numImages, &numImages, imageData->GetColorImageArray())));
    }

    pollEvent(instance.get(), d);

    xr::UniqueDynamicSpace view;
    // Populate spaces
    {
        view = session->createReferenceSpaceUnique(
            xr::ReferenceSpaceCreateInfo{xr::ReferenceSpaceType::View, xr::Posef{}}, d);
    }

    std::cout << "Wait for xr::SessionState::Ready\n";
    while (state != xr::SessionState::Ready) {
        pollEvent(instance.get(), d);
    }

    // Begin session

    std::cout << "Begin Session\n";
    session->beginSession({viewConfigType}, d);

    xr::EnvironmentBlendMode blendMode = blendModes.front();
    // "Frame" loop
    int frameNum = 0;

    std::vector<xr::CompositionLayerProjectionView> projectionViews(numViews);
    std::vector<xr::View> views(numViews);
    for (uint32_t i = 0; i < numViews; ++i) {
        projectionViews[i].subImage = xr::SwapchainSubImage{get(swapchain), rect, i};
    }
    while (!quitKeyPressed) {
        ++frameNum;
        pollEvent(instance.get(), d);
        xr::Time t;

        // wait frame - gives us our time
        auto frameState = session->waitFrame(xr::FrameWaitInfo{});
        t = frameState.predictedDisplayTime;

        // begin frame
        xr::Result ret = session->beginFrame(xr::FrameBeginInfo{}, d);
        if (ret == xr::Result::SessionLossPending) {
            // Can't really do too much.
            return 1;
        }

        {
            if (frameNum % 10 == 0) {
                std::cout << "Iteration: " << frameNum << "\n";
            }

            if (frameState.shouldRender) {
                xr::ViewState viewState;
                views =
                    session->locateViewsToVector(xr::ViewLocateInfo{viewConfigType, t, get(view)}, put(viewState), d);

                auto swapchainImageIndex = swapchain->acquireSwapchainImage({}, d);
                XRC_CHECK_THROW_XRCMD(
                    get(swapchain->waitSwapchainImage(xr::SwapchainImageWaitInfo{xr::Duration::infinite()}, d)));

                auto whichComponent = frameNum % 3;
                XrColor4f color{(whichComponent == 0) ? 1.f : 0.f, (whichComponent == 1) ? 1.f : 0.f,
                                (whichComponent == 2) ? 1.f : 0.f, 1.f};
                for (uint32_t i = 0; i < numViews; ++i) {

                    graphicsPlugin->ClearImageSlice(imageData->GetGenericColorImage(swapchainImageIndex), i, color);
                    projectionViews[i].fov = views[i].fov;
                    projectionViews[i].pose = views[i].pose;
                }
                swapchain->releaseSwapchainImage({});

                auto layer = xr::CompositionLayerProjection{{}, get(view), numViews, projectionViews.data()};
                std::array<const xr::CompositionLayerBaseHeader *, 1> layers{{&layer}};
                session->endFrame(xr::FrameEndInfo{t, blendMode, 1, layers.data()}, d);
            } else {
                // end frame
                session->endFrame(xr::FrameEndInfo{t, blendMode, 0, nullptr}, d);
            }
        }
    }
    session->requestExitSession(d);
    session->endSession(d);

    return 0;
}
