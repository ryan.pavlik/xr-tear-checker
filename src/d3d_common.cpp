// Copyright (c) 2017-2022, The Khronos Group Inc.
//
// SPDX-License-Identifier: Apache-2.0

#if (defined(XR_USE_GRAPHICS_API_D3D11) || defined(XR_USE_GRAPHICS_API_D3D12)) && !defined(MISSING_DIRECTX_COLORS)

#include <D3Dcompiler.h>
#include <DirectXColors.h>
#include <wrl/client.h> // For Microsoft::WRL::ComPtr

#include "d3d_common.h"
#include "throw_helpers.h"

using namespace Microsoft::WRL;
using namespace DirectX;

XMMATRIX XM_CALLCONV LoadXrPose(const XrPosef &pose) {
    return XMMatrixAffineTransformation(DirectX::g_XMOne, DirectX::g_XMZero,
                                        XMLoadFloat4(reinterpret_cast<const XMFLOAT4 *>(&pose.orientation)),
                                        XMLoadFloat3(reinterpret_cast<const XMFLOAT3 *>(&pose.position)));
}

bool operator==(LUID luid, uint64_t id) { return ((((uint64_t)luid.HighPart << 32) | (uint64_t)luid.LowPart) == id); }

// If adapterId is 0 then use the first adapter we find, the default adapter.
ComPtr<IDXGIAdapter1> GetDXGIAdapter(LUID adapterId) noexcept(false) {
    ComPtr<IDXGIAdapter1> dxgiAdapter;
    ComPtr<IDXGIFactory1> dxgiFactory;

    HRESULT hr =
        CreateDXGIFactory1(__uuidof(IDXGIFactory1), reinterpret_cast<void **>(dxgiFactory.ReleaseAndGetAddressOf()));
    XRC_CHECK_THROW_HRESULT(hr, "GetAdapter: CreateDXGIFactory1");

    for (UINT adapterIndex = 0;; adapterIndex++) {
        // EnumAdapters1 will fail with DXGI_ERROR_NOT_FOUND when there are no more adapters to enumerate.
        hr = dxgiFactory->EnumAdapters1(adapterIndex, dxgiAdapter.ReleaseAndGetAddressOf());

        DXGI_ADAPTER_DESC1 adapterDesc;
        hr = dxgiAdapter->GetDesc1(&adapterDesc);
        XRC_CHECK_THROW_HRESULT(hr, "dxgiAdapter->GetDesc1");

        if ((adapterId == 0) || memcmp(&adapterDesc.AdapterLuid, &adapterId, sizeof(adapterId)) == 0) {
            return dxgiAdapter;
        }
    }
}

#endif
