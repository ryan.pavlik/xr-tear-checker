// Copyright (c) 2019-2022, The Khronos Group Inc.
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "graphics_plugin.h"
#include <openxr/openxr.h>

#if defined(XR_USE_GRAPHICS_API_D3D11) && !defined(MISSING_DIRECTX_COLORS)

#include "swapchain_image_data.h"
#include "throw_helpers.h"
#include "util.h"
#include <algorithm>
#include <array>
#include <d3d11.h>
#include "d3d_common.h"
#include <openxr/openxr_platform.h>
#include <windows.h>
#include <wrl/client.h> // For Microsoft::WRL::ComPtr

#include "d3d_common.h"

using namespace Microsoft::WRL;

struct D3D11FallbackDepthTexture {
  public:
    D3D11FallbackDepthTexture() = default;

    void Reset() {
        m_texture = nullptr;
        m_xrImage.texture = nullptr;
    }
    bool Allocated() const { return m_texture != nullptr; }

    void Allocate(ID3D11Device *d3d11Device, UINT width, UINT height, UINT arraySize) {
        Reset();
        D3D11_TEXTURE2D_DESC depthDesc{};
        depthDesc.Width = width;
        depthDesc.Height = height;
        depthDesc.ArraySize = arraySize;
        depthDesc.MipLevels = 1;
        depthDesc.Format = DXGI_FORMAT_R32_TYPELESS;
        depthDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_DEPTH_STENCIL;
        depthDesc.SampleDesc.Count = 1;
        XRC_CHECK_THROW_HRCMD(d3d11Device->CreateTexture2D(&depthDesc, nullptr, m_texture.ReleaseAndGetAddressOf()));
        m_xrImage.texture = m_texture.Get();
    }

    const XrSwapchainImageD3D11KHR &GetTexture() const { return m_xrImage; }

  private:
    ComPtr<ID3D11Texture2D> m_texture{};
    XrSwapchainImageD3D11KHR m_xrImage{XR_TYPE_SWAPCHAIN_IMAGE_D3D11_KHR, NULL, nullptr};
};

class D3D11SwapchainImageData : public SwapchainImageDataBase<XrSwapchainImageD3D11KHR> {
  public:
    D3D11SwapchainImageData(ComPtr<ID3D11Device> device, uint32_t capacity, const XrSwapchainCreateInfo &createInfo,
                            XrSwapchain depthSwapchain, const XrSwapchainCreateInfo &depthCreateInfo)
        : SwapchainImageDataBase(XR_TYPE_SWAPCHAIN_IMAGE_D3D11_KHR, capacity, createInfo, depthSwapchain,
                                 depthCreateInfo),
          m_device(std::move(device))

    {}
    D3D11SwapchainImageData(ComPtr<ID3D11Device> device, uint32_t capacity, const XrSwapchainCreateInfo &createInfo)
        : SwapchainImageDataBase(XR_TYPE_SWAPCHAIN_IMAGE_D3D11_KHR, capacity, createInfo), m_device(std::move(device)),
          m_internalDepthTextures(capacity)

    {}

    void Reset() override {
        m_internalDepthTextures.clear();
        m_device = nullptr;
        SwapchainImageDataBase::Reset();
    }

    const XrSwapchainImageD3D11KHR &GetFallbackDepthSwapchainImage(uint32_t i) override {

        if (!m_internalDepthTextures[i].Allocated()) {
            m_internalDepthTextures[i].Allocate(m_device.Get(), this->Width(), this->Height(), this->ArraySize());
        }

        return m_internalDepthTextures[i].GetTexture();
    }

  private:
    ComPtr<ID3D11Device> m_device;
    std::vector<D3D11FallbackDepthTexture> m_internalDepthTextures;
};

struct D3D11GraphicsPlugin : public IGraphicsPlugin {
  public:
    D3D11GraphicsPlugin();

    ~D3D11GraphicsPlugin() override;

    bool Initialize() override;

    bool IsInitialized() const override;

    void Shutdown() override;

    std::string DescribeGraphics() const override;

    std::vector<std::string> GetInstanceExtensions() const override;

    bool InitializeDevice(XrInstance instance, XrSystemId systemId, bool checkGraphicsRequirements,
                          uint32_t deviceCreationFlags) override;

    void Flush() override;

    void ClearSwapchainCache() override;

    void ShutdownDevice() override;

    const XrBaseInStructure *GetGraphicsBinding() const override;

    // Format required by RGBAImage type.
    int64_t GetSRGBA8Format() const override;

    ISwapchainImageData *AllocateSwapchainImageData(size_t size,
                                                    const XrSwapchainCreateInfo &swapchainCreateInfo) override;

    void ClearImageSlice(const XrSwapchainImageBaseHeader *colorSwapchainImage, uint32_t imageArrayIndex,
                         XrColor4f bgColor = DarkSlateGrey) override;

  private:
    ComPtr<ID3D11RenderTargetView> CreateRenderTargetView(D3D11SwapchainImageData &swapchainData, uint32_t imageIndex,
                                                          uint32_t imageArrayIndex) const;
    ComPtr<ID3D11DepthStencilView> CreateDepthStencilView(D3D11SwapchainImageData &swapchainData, uint32_t imageIndex,
                                                          uint32_t imageArrayIndex) const;

    bool initialized{false};
    XrGraphicsBindingD3D11KHR graphicsBinding;
    ComPtr<ID3D11Device> d3d11Device;
    ComPtr<ID3D11DeviceContext> d3d11DeviceContext;

    SwapchainImageDataMap<D3D11SwapchainImageData> m_swapchainImageDataMap;
};

D3D11GraphicsPlugin::D3D11GraphicsPlugin()
    : initialized(false), graphicsBinding{XR_TYPE_GRAPHICS_BINDING_D3D11_KHR}, d3d11Device(), d3d11DeviceContext() {}

D3D11GraphicsPlugin::~D3D11GraphicsPlugin() {
    ShutdownDevice();
    Shutdown();
}

bool D3D11GraphicsPlugin::Initialize() {
    if (initialized)
        return false;

    // To do.
    initialized = true;
    return initialized;
}

bool D3D11GraphicsPlugin::IsInitialized() const { return initialized; }

void D3D11GraphicsPlugin::Shutdown() {
    if (initialized) {
        // To do.
        initialized = false;
    }
}

std::string D3D11GraphicsPlugin::DescribeGraphics() const { return std::string("D3D11"); }

std::vector<std::string> D3D11GraphicsPlugin::GetInstanceExtensions() const {
    return {XR_KHR_D3D11_ENABLE_EXTENSION_NAME};
}

bool D3D11GraphicsPlugin::InitializeDevice(XrInstance instance, XrSystemId systemId, bool checkGraphicsRequirements,
                                           uint32_t deviceCreationFlags) {
    try {
        XrGraphicsRequirementsD3D11KHR graphicsRequirements{
            XR_TYPE_GRAPHICS_REQUIREMENTS_D3D11_KHR, nullptr, {0, 0}, D3D_FEATURE_LEVEL_11_0};

        // Create the D3D11 device for the adapter associated with the system.
        if (checkGraphicsRequirements) {

            auto xrGetD3D11GraphicsRequirementsKHR =
                GetInstanceExtensionFunction<PFN_xrGetD3D11GraphicsRequirementsKHR>(
                    instance, "xrGetD3D11GraphicsRequirementsKHR");

            XrResult result = xrGetD3D11GraphicsRequirementsKHR(instance, systemId, &graphicsRequirements);
            if (XR_FAILED(result)) {
                // Log result?
                return false;
            }
        }

        const ComPtr<IDXGIAdapter1> adapter = GetDXGIAdapter(graphicsRequirements.adapterLuid);

        // Create a list of feature levels which are both supported by the OpenXR runtime and this application.
        std::vector<D3D_FEATURE_LEVEL> featureLevels = {D3D_FEATURE_LEVEL_12_1, D3D_FEATURE_LEVEL_12_0,
                                                        D3D_FEATURE_LEVEL_11_1, D3D_FEATURE_LEVEL_11_0,
                                                        D3D_FEATURE_LEVEL_10_1, D3D_FEATURE_LEVEL_10_0};
        featureLevels.erase(
            std::remove_if(featureLevels.begin(), featureLevels.end(),
                           [&](D3D_FEATURE_LEVEL fl) { return (fl < graphicsRequirements.minFeatureLevel); }),
            featureLevels.end());

        if (featureLevels.empty()) {
            // Log result?
            return false;
        }

        // Create the device
        UINT creationFlags = deviceCreationFlags | D3D11_CREATE_DEVICE_BGRA_SUPPORT;
#if !defined(NDEBUG)
        creationFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

        // Create the Direct3D 11 API device object and a corresponding context.
        const D3D_DRIVER_TYPE driverType = ((adapter == nullptr) ? D3D_DRIVER_TYPE_HARDWARE : D3D_DRIVER_TYPE_UNKNOWN);

    TryAgain:
        HRESULT hr =
            D3D11CreateDevice(adapter.Get(), driverType, 0, creationFlags, featureLevels.data(),
                              (UINT)featureLevels.size(), D3D11_SDK_VERSION, d3d11Device.ReleaseAndGetAddressOf(),
                              nullptr, d3d11DeviceContext.ReleaseAndGetAddressOf());
        if (FAILED(hr)) {
            if (creationFlags & D3D11_CREATE_DEVICE_DEBUG) // This can fail if debug functionality is not installed.
            {
                creationFlags &= ~D3D11_CREATE_DEVICE_DEBUG;
                goto TryAgain;
            }

            // If the initialization fails, fall back to the WARP device.
            // For more information on WARP, see: http://go.microsoft.com/fwlink/?LinkId=286690
            hr = D3D11CreateDevice(nullptr, D3D_DRIVER_TYPE_WARP, 0, creationFlags, featureLevels.data(),
                                   (UINT)featureLevels.size(), D3D11_SDK_VERSION, d3d11Device.ReleaseAndGetAddressOf(),
                                   nullptr, d3d11DeviceContext.ReleaseAndGetAddressOf());

            if (FAILED(hr)) {
                // Log it?
                return false;
            }
        }

        graphicsBinding.device = d3d11Device.Get();

        return true;
    } catch (...) {
        // Log it?
    }

    return false;
}

void D3D11GraphicsPlugin::ClearSwapchainCache() { m_swapchainImageDataMap.Reset(); }

void D3D11GraphicsPlugin::Flush() {
    // https://docs.microsoft.com/en-us/windows/win32/api/d3d11/nf-d3d11-id3d11devicecontext-flush
    if (d3d11DeviceContext)
        d3d11DeviceContext->Flush();
}

void D3D11GraphicsPlugin::ShutdownDevice() {
    graphicsBinding = XrGraphicsBindingD3D11KHR{XR_TYPE_GRAPHICS_BINDING_D3D11_KHR};

    m_swapchainImageDataMap.Reset();

    d3d11DeviceContext.Reset();
    d3d11Device.Reset();
}

const XrBaseInStructure *D3D11GraphicsPlugin::GetGraphicsBinding() const {
    if (graphicsBinding.device) {
        return reinterpret_cast<const XrBaseInStructure *>(&graphicsBinding);
    }
    return nullptr;
}

int64_t D3D11GraphicsPlugin::GetSRGBA8Format() const { return DXGI_FORMAT_R8G8B8A8_UNORM_SRGB; }

ISwapchainImageData *D3D11GraphicsPlugin::AllocateSwapchainImageData(size_t size,
                                                                     const XrSwapchainCreateInfo &swapchainCreateInfo) {
    auto typedResult = std::make_unique<D3D11SwapchainImageData>(d3d11Device, uint32_t(size), swapchainCreateInfo);

    // Cast our derived type to the caller-expected type.
    auto ret = static_cast<ISwapchainImageData *>(typedResult.get());

    m_swapchainImageDataMap.Adopt(std::move(typedResult));

    return ret;
}

ComPtr<ID3D11RenderTargetView> D3D11GraphicsPlugin::CreateRenderTargetView(D3D11SwapchainImageData &swapchainData,
                                                                           uint32_t imageIndex,
                                                                           uint32_t imageArrayIndex) const {

    // Create RenderTargetView with original swapchain format (swapchain is typeless).
    ComPtr<ID3D11RenderTargetView> renderTargetView;
    const CD3D11_RENDER_TARGET_VIEW_DESC renderTargetViewDesc(
        (swapchainData.SampleCount() > 1) ? D3D11_RTV_DIMENSION_TEXTURE2DMSARRAY : D3D11_RTV_DIMENSION_TEXTURE2DARRAY,
        (DXGI_FORMAT)swapchainData.GetCreateInfo().format, 0 /* mipSlice */, imageArrayIndex, 1 /* arraySize */);

    ID3D11Texture2D *const colorTexture = swapchainData.GetTypedImage(imageIndex).texture;

    XRC_CHECK_THROW_HRCMD(d3d11Device->CreateRenderTargetView(colorTexture, &renderTargetViewDesc,
                                                              renderTargetView.ReleaseAndGetAddressOf()));
    return renderTargetView;
}

ComPtr<ID3D11DepthStencilView> D3D11GraphicsPlugin::CreateDepthStencilView(D3D11SwapchainImageData &swapchainData,
                                                                           uint32_t imageIndex,
                                                                           uint32_t imageArrayIndex) const {

    // Clear depth buffer.
    const ComPtr<ID3D11Texture2D> depthStencilTexture = swapchainData.GetDepthImageForColorIndex(imageIndex).texture;
    ComPtr<ID3D11DepthStencilView> depthStencilView;
    DXGI_FORMAT depthSwapchainFormatDX = DXGI_FORMAT_D32_FLOAT;
    uint32_t depthArraySize = 1;
    const XrSwapchainCreateInfo *depthCreateInfo = swapchainData.GetDepthCreateInfo();
    if (depthCreateInfo != nullptr) {
        depthSwapchainFormatDX = (DXGI_FORMAT)depthCreateInfo->format;
        depthArraySize = depthCreateInfo->arraySize;
    }
    CD3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc(
        (swapchainData.DepthSampleCount() > 1) ? D3D11_DSV_DIMENSION_TEXTURE2DMSARRAY
                                               : D3D11_DSV_DIMENSION_TEXTURE2DARRAY,
        depthSwapchainFormatDX, 0 /* mipSlice */, imageArrayIndex, depthArraySize);
    XRC_CHECK_THROW_HRCMD(d3d11Device->CreateDepthStencilView(depthStencilTexture.Get(), &depthStencilViewDesc,
                                                              depthStencilView.GetAddressOf()));
    return depthStencilView;
}

void D3D11GraphicsPlugin::ClearImageSlice(const XrSwapchainImageBaseHeader *colorSwapchainImage,
                                          uint32_t imageArrayIndex, XrColor4f bgColor) {

    D3D11SwapchainImageData *swapchainData;
    uint32_t imageIndex;

    std::tie(swapchainData, imageIndex) = m_swapchainImageDataMap.GetDataAndIndexFromBasePointer(colorSwapchainImage);

    // Clear color buffer.
    // Create RenderTargetView with original swapchain format (swapchain is typeless).
    ComPtr<ID3D11RenderTargetView> renderTargetView =
        CreateRenderTargetView(*swapchainData, imageIndex, imageArrayIndex);
    // TODO: Do not clear to a color when using a pass-through view configuration.
    FLOAT bg[] = {bgColor.r, bgColor.g, bgColor.b, bgColor.a};
    d3d11DeviceContext->ClearRenderTargetView(renderTargetView.Get(), bg);

    // Clear depth buffer.
    ComPtr<ID3D11DepthStencilView> depthStencilView =
        CreateDepthStencilView(*swapchainData, imageIndex, imageArrayIndex);
    d3d11DeviceContext->ClearDepthStencilView(depthStencilView.Get(), D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
}

std::shared_ptr<IGraphicsPlugin> CreateGraphicsPlugin_D3D11() { return std::make_shared<D3D11GraphicsPlugin>(); }

#endif // XR_USE_GRAPHICS_API_D3D11
